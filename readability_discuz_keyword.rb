require "rubygems"
require 'readability'
require 'open-uri'
require 'net/http'

url = "http://my.eoe.cn/iceskysl/archive/898.html"
source = open(url).read
source_content = Readability::Document.new(source)

data = {
  "title" => source_content.title,
  "content" => source_content.content,
  "ics" => "utf-8",
  "ocs" => "utf-8"
}

puts data['title']
puts data['content']

#调用discuz的一个开放接口，传入datas返回关键词
url = URI.parse('http://keyword.discuz.com/related_kw.html')
Net::HTTP.start(url.host, url.port) do |http|
 req = Net::HTTP::Post.new(url.path)
 req.set_form_data(data)
 puts http.request(req).body
end

=begin
<?xml version="1.0" encoding="utf-8" ?>
<total_response>
	<svalid>36000</svalid>
	<keyword>
	<info>
		<count>5</count>
		<errno>0</errno>
		<nextuptime>1291287160</nextuptime>
		<keep>0</keep>
	</info>
	<result>
		<item>
			<kw><![CDATA[的]]></kw>
		</item>
		<item>
			<kw><![CDATA[二维码]]></kw>
		</item>
		<item>
			<kw><![CDATA[浏览器]]></kw>
		</item>
		<item>
			<kw><![CDATA[开发者]]></kw>
		</item>
		<item>
			<kw><![CDATA[网页]]></kw>
		</item>
	</result>
	</keyword>
</total_response>
=end